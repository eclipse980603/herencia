package programa;

public class ProductoFresco extends Producto {
	private String fechaEnvasado;
	private String paisOrigen;
	
	public ProductoFresco(String fechaCaducidad, int numeroLote, String fechaEnvasado, String paisOrigen) {
		super(fechaCaducidad, numeroLote);
		setFechaEnvasado(fechaEnvasado);
		setPaisOrigen(paisOrigen);
	}
	
	
	private void setFechaEnvasado(String fechaEnvasado) {
		this.fechaEnvasado = fechaEnvasado;
	}
	
	
	public String getFechaEnvasado() {
		return fechaEnvasado;
	}
	
	
	private void setPaisOrigen(String paisOrigen) {
		this.paisOrigen = paisOrigen;
	}
	
	
	public String getPaisOrigen() {
		return paisOrigen;
	}
	
	
	public String toString(){
		return  "El producto " + getNumeroLote() + " con fecha de caducidad " + getFechaCaducidad() + ". Es un Producto fresco que fue envasado en: " + getFechaEnvasado() + " con fecha de caducidad" + getFechaCaducidad() + " en " + getPaisOrigen();
	}
	
	
}
