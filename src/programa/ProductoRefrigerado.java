package programa;

public class ProductoRefrigerado extends Producto {
	private String codeOrganization;
	
	public ProductoRefrigerado(String fechaCaducidad, int numeroLote, String codeOrganization) {
		super(fechaCaducidad, numeroLote);
		setCodeOrganization(codeOrganization);
	}
	
	private void setCodeOrganization(String codeOrganization) {
		this.codeOrganization = codeOrganization;
	}
	public String getCodeOrganization() {
		return codeOrganization;
	}
	
	
	public String toString(){
		return "El producto " + getNumeroLote() + " con fecha de caducidad " + getFechaCaducidad() + ". Es un producto refrigerado, con un codigo de Organización de " + getCodeOrganization();
	}
}
