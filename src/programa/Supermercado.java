package programa;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Supermercado {
	private static ArrayList<ProductoCongelado> listraCongelados;
	private static ArrayList<ProductoFresco> listraFresco;
	private static ArrayList<ProductoRefrigerado> listraRefrigerado;

	public static void main(String[] args) {
		Scanner read = new Scanner(System.in);
		ProductoCongelado productoCon;
		ProductoFresco productoFes;
		ProductoRefrigerado productoRef;
		
		listraCongelados = new ArrayList<ProductoCongelado>();
		listraFresco = new ArrayList<ProductoFresco>();
		listraRefrigerado = new ArrayList<ProductoRefrigerado>();
		System.out.println("Bienvenido al Supermercado");
		
		fillProductos();
		boolean bucle = true;
		while(bucle) {
			
			System.out.println("\n¿Que desea hacer?\n"
					+"1- Mirar la sección de Alimentos Congelados\n"
					+"2- Mirar la sección de Alimentos Fescos\n"
					+"3- Mirar la sección de Alimentos Refrigerados\n"
					+"4- Irse \n");
			switch(read.nextLine()){
			case("1"):
				for(int i=0; i<listraCongelados.size();i++) {
					productoCon = listraCongelados.get(i);
					System.out.println(productoCon);
				}
			continue;
			case("2"):
				for(int i=0; i<listraFresco.size();i++) {
					productoFes = listraFresco.get(i);
					System.out.println(productoFes);
				}
			continue;
			case("3"):
				for(int i=0; i<listraRefrigerado.size();i++) {
					productoRef = listraRefrigerado.get(i);
					System.out.println(productoRef);
				}
			continue;
			case("4"):
				System.out.println();
				System.out.println("¡Nos vemos otro dia!");
				System.out.println();
				bucle = false;
				break;
			default:
				System.out.println();
				System.out.println("¡Error, introduce un valor valido!");
				System.out.println();
				continue;
			}
		}
	read.close();
	}
	
	public static void fillProductos(){
		Random random = new Random();
		LocalDate dia = LocalDate.now();
		int numLoteCon,numLoteFes,numLoteRef,numTemperatura,numPais;
		String fechacad,fechaenv,temperatura;
		String[] paisOrigen = {"Islandia","America","Japon","Italia","España","Alemania","Francia","Argentina","Turkia","Ruisa"}; 
		
		for (int i=0; i<5;i++) {
			fechacad = dia.toString();
			fechacad.replace('4', '6');
			fechaenv = dia.toString();
			fechaenv.replace('4', '3');
			
			numLoteCon = random.nextInt(999);
			numLoteFes = random.nextInt(999);
			numLoteRef = random.nextInt(999);
			numTemperatura = random.nextInt(20)-10;
			numPais = random.nextInt(9);
			temperatura = String.valueOf(numTemperatura);
	
			ProductoCongelado productoCon = new ProductoCongelado(fechacad,numLoteCon,temperatura);
			ProductoFresco productoFes = new ProductoFresco(fechacad,numLoteFes,paisOrigen[numPais],fechaenv);
			ProductoRefrigerado productoRef = new ProductoRefrigerado(fechacad,numLoteRef,"L40 13:19 S4140"); //Paso de hacer esto aleatorio
			
			listraCongelados.add(productoCon);
			listraFresco.add(productoFes);
			listraRefrigerado.add(productoRef);
		}
	}
	

}
