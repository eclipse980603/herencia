package programa;

public class Producto {
	public String fechaCaducidad;
	public int numeroLote;
	
	public Producto(String fechaCaducidad,int numeroLote) {
		setFechaCaducidad(fechaCaducidad);
		setNumeroLote(numeroLote);
		
	}
	
	private void setFechaCaducidad(String fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}
	
	
	public String getFechaCaducidad() {
		return fechaCaducidad;
	}
	
	
	private void setNumeroLote(int numeroLote) {
		this.numeroLote = numeroLote;
	}
	
	
	public int getNumeroLote() {
		return numeroLote;
	}
	
}

	