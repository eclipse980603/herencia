package programa;

public class ProductoCongelado extends Producto {

	private String recomendedTemperature;
	
	
	public ProductoCongelado(String fechaCaducidad, int numeroLote, String recomendedTemperature) {
		super(fechaCaducidad, numeroLote);
		setRecomendedTemperature(recomendedTemperature);
	}
	
	private void setRecomendedTemperature(String recomendedTemperature){
		this.recomendedTemperature = recomendedTemperature;
	}
	
	
	public String getRecomendedTemperature() {
		return recomendedTemperature;
		
	}
	
	public String toString(){
		return "El producto " + getNumeroLote() + " con fecha de caducidad " + getFechaCaducidad() + ". Es un producto congelado, se recomienda que esté a " + getRecomendedTemperature() + " temperatura ";
	}
	
}
